import React from "react";
import { connect } from "react-redux";
import { selectSong } from "../actions";
import {
  Button,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading
} from "shards-react";
class SongList extends React.Component {
  renderList() {
    return this.props.songsReducer.map(song => {
      return (
        <ListGroupItem key={song.title}>
          <ListGroupItemHeading>
            {song.author} - {song.title}
          </ListGroupItemHeading>
          <Button
            onClick={() => {
              this.props.selectSong(song);
            }}
          >
            More details
          </Button>
        </ListGroupItem>
      );
    });
  }

  render() {
    return <ListGroup>{this.renderList()}</ListGroup>;
  }
}

const mapStateToProps = state => {
  console.log(state);
  return { songsReducer: state.songsReducer };
};

export default connect(mapStateToProps, {
  selectSong
})(SongList);
