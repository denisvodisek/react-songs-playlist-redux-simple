import React from "react";
import { connect } from "react-redux";
import {
  Card,
  CardHeader,
  CardTitle,
  CardImg,
  CardBody,
  CardFooter
} from "shards-react";
const SongDetails = ({ song }) => {
  return (
    <>
      {song && (
        <Card style={{ maxWidth: "300px" }}>
          <CardHeader>{song.author}</CardHeader>
          <CardImg src={song.cover} />
          <CardBody>
            <CardTitle>{song.title}</CardTitle>
            <p>{song.description}</p>
          </CardBody>
          <CardFooter>{song.duration}</CardFooter>
        </Card>
      )}
    </>
  );
};

const mapStateToProps = state => {
  return {
    song: state.selectedSongReducer
  };
};

export default connect(mapStateToProps)(SongDetails);
