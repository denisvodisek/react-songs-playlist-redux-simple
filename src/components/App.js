import React from "react";
import SongList from "./SongList";
import { Container, Row, Col } from "shards-react";
import SongDetails from "./SongDetails";
const App = () => {
  return (
    <Container>
      <br></br>
      <Row>
        <Col>
          <h1>Denis's playlist</h1>
          <br></br>
        </Col>
      </Row>
      <Row>
        <Col>
          <SongList />
        </Col>
        <Col>
          <SongDetails />
        </Col>
      </Row>
    </Container>
  );
};

export default App;
