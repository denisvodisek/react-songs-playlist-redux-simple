import { combineReducers } from "redux";

const songsReducer = () => {
  return [
    {
      author: "Drake",
      description:
        "Hotline Bling is a song recorded by Canadian rapper Drake, which serves as the lead single from his fourth studio album Views (2016)",
      title: "Hotline Bling",
      cover:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Drake_-_Hotline_Bling.png/330px-Drake_-_Hotline_Bling.png",
      duration: "3:30"
    },
    {
      author: "Rick Ross",
      description: "These haters can't hold me back.",
      title: "Hold me back",
      cover: "https://i.ytimg.com/vi/HVO5WhIm4uI/maxresdefault.jpg",
      duration: "4:19"
    },
    {
      author: "Rick Ross",
      description: "Some things you have to see for yourself ",
      title: "Family ties",
      cover:
        "https://hiphopsince1987.com/wp-content/uploads/2015/07/CKhzDumUYAEvDd6-1.jpg",
      duration: "3:49"
    }
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === "SONG_SELECTED") {
    return action.payload;
  }

  return selectedSong;
};

export default combineReducers({
  songsReducer,
  selectedSongReducer
});
